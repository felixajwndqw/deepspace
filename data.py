from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.automap import automap_base
import cv2
import glob
import os
import numpy as np


FEATURES = {words[0]: int(words[1]) for words in [line.split(",") for line in open("features.txt").readlines()]}


def get_db():
    Base = automap_base()
    engine = create_engine('postgresql://localhost/gc')
    Base.prepare(engine, reflect=True)

    Galaxy = Base.classes.galaxy
    Annotation = Base.classes.annotation
    Shape = Base.classes.shape

    Session = sessionmaker(bind=engine)
    session = Session()

    def g(self):
        return '<Galaxy ID: {}. Name: {}.>'.format(self.g_id, self.name)

    def a(self):
        return '<Annotation ID: {}. Galaxy ID: {}. Timestamp: {}>'.format(self.a_id, self.g_id, self.timestamp)

    def s(self):
        return '<Shape ID: {}. Annotation ID: {}. Shape: {}>'.format(self.s_id, self.a_id, self.shape)

    Galaxy.__repr__ = g
    Annotation.__repr__ = a
    Shape.__repr__ = s

    return session, Galaxy, Annotation, Shape


def get_all_images(sims=['a', 'b', 'c', 'd']):
    imgs = []
    headers = []
    owd = os.getcwd()

    if sims is str:
        sims = [sims]

    for sim in sims:
        os.chdir(sim)
        imgs.append([cv2.imread(f) for f in glob.glob('*.png')])
        os.chdir(owd)

    os.chdir(sim[0])
    headers = [parse_header(open(f).read()) for f in glob.glob("*.hhh")]

    return np.array(imgs), headers


def get_images(n, surveys=['a', 'b', 'c', 'd']):
    surveys = surveys.split(",")
    if surveys is str:
        surveys = [surveys]

    if surveys[0][:3] == 'sim':
        n = int(n)
        chars = [s[-1] for s in surveys]
        imgs = np.array([cv2.imread(sim+"//"+sim+'%03d.png' % n) for sim in chars])

    header = parse_header(open(chars[0]+'//'+chars[0]+'%03d.hhh' % n).read())

    return np.array(imgs), header


def is_digit(x):
    try:
        float(x)
        return True
    except ValueError:
        return False


def show_image(img, title=""):
    cv2.imshow(title, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def parse_header(text):
    lines = [text[start:start+80] for start in range(0, len(text), 80)]
    header = {}
    for line in lines:
        key = line.split(' ', 1)[0]
        val = line[:30].rsplit(' ', 1)[1]
        if is_digit(val):
            val = float(val)
        header[key] = val
    return header


def world_to_pixel(p, header):
    x = header['CRPIX1']-(header['CRVAL1']-p[0])/header['CDELT1']
    y = header['CRPIX2']+(header['CRVAL2']-p[1])/header['CDELT2']
    return [round(x), round(y)]


def feature_to_colour(f):
    feature_hue = np.uint8([[[FEATURES[f]*179/13, 255, 255]]])
    color = (cv2.cvtColor(feature_hue, cv2.COLOR_HSV2BGR))[0, 0]
    return tuple(int(v) for v in color)


def convert_shape(shape, header=None):
    new_shape = {}
    new_shape['s_id'] = shape.s_id
    new_shape['a_id'] = shape.a_id
    new_shape['shape'] = shape.shape
    new_shape['number'] = shape.number
    new_shape['feature'] = shape.feature
    new_shape['colour'] = feature_to_colour(shape.feature)
    print(new_shape['feature'])
    print(new_shape['colour'])
    if shape.shape == "Region":
        new_shape['points'] = np.array([world_to_pixel((ra, dec), header) for ra, dec in zip(shape.ra_points, shape.dec_points)])

    return new_shape


def visualise_annotation(imgs, shapes):
    for img in imgs:
        for s in shapes:
            pts = s['points'].reshape((-1, 1, 2))
            cv2.polylines(img, [pts], True, s['colour'])

    if imgs.shape[0] == 4:
        big_img = np.vstack((np.hstack((imgs[0], imgs[1])), np.hstack((imgs[2], imgs[3]))))
    show_image(big_img)


def create_annotation(a_id):
    a = (session.query(Annotation).filter(Annotation.a_id == a_id))[0]
    g = (session.query(Galaxy).filter(Galaxy.g_id == a.g_id))[0]
    shapes = session.query(Shape).filter(Shape.a_id == a.a_id)

    imgs, header = get_images(g.name, g.survey)

    print(g)
    print(header)
    converted_shapes = []
    for s in shapes:
        converted_shapes.append(convert_shape(s, header))

    visualise_annotation(imgs, converted_shapes)


if __name__ == "__main__":
    session, Galaxy, Annotation, Shape = get_db()
    gs = session.query(Galaxy).filter(Galaxy.survey == "sim_a,sim_b,sim_c,sim_d")
    annotations = session.query(Annotation).all()

    for a in annotations:
        create_annotation(a.a_id)
