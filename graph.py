import numpy as np
from scipy.spatial.distance import pdist, squareform


def knn(k, dists):
    N = dists.shape[0]
    G = np.zeros_like(dists)
    idxs = dists.argsort()
    for i in range(N):
        for j in range(N):
            if j > 0 and j <= k:
                G[i, idxs[i, j]] = 1
            if j > k:
                G[i, idxs[i, j]] = 0
    return np.maximum(G, G.T)


def eneighbour(e, dists):
    G = np.zeros((dists.shape[0], dists.shape[0]))
    G = [[1 if x < e else 0 for x in row] for row in dists]
    return G


def gaussian_weight(dist, sigma):
    return np.exp(-dist/2*sigma**2)


def create_weight_matrix(data, density_invariant=False, weight_function="gaussian", neighbour_function="knn"):
    # if weight_function is "gaussian":
    weight = gaussian_weight

    N = data.shape[0]
    euc_dists = squareform(pdist(data))
    sigma = np.median(euc_dists)
    if neighbour_function == "eps":
        G = eneighbour(sigma, euc_dists)
    elif neighbour_function == "knn":
        G = knn(5, euc_dists)
    
    sigma = np.mean(np.multiply(euc_dists, G))
    W = np.array([[weight(euc_dists[i, j], sigma) for j in range(N)] for i in range(N)])
    W = np.multiply(W, G)

    if density_invariant:
        Q = np.sum(W, axis=0)
        W = [[W[i, j]/(Q[i]*Q[j]) for j in range(N)] for i in range(N)]
        
    return W, sigma


def create_degree_matrix(W):
    return np.diag(np.sum(W, axis=0))