import numpy as np
import graph


class Eigenmap:
    def __init__(self, data):
        self.W = graph.create_weight_matrix(data)
        self.D = graph.create_degree_matrix(self.W)
        self.L = create_laplacian_matrix(self.W, self.D)
        self.orig_dim = data.shape[1]
        self.X, self.eig_values, self.eig_vectors = generate_mapping(self.L, self.orig_dim)

    def get_map(self, q):
        return self.X[:, :q]


def create_laplacian_matrix(W, D):
    N = W.shape[0]
    I = np.identity(N)
    DWD = np.empty_like(I)
    for i in range(N):
        for j in range(N):
            DWD[i, j] = np.divide(-W[i, j], np.sqrt(D[i, i]*D[j, j]))
    return np.subtract(I, DWD)


def generate_mapping(L, q):
    eig_values, eig_vectors = np.linalg.eig(L)
    idxs = eig_values.argsort()
    eig_values = eig_values[idxs[::-1]]
    eig_vectors = eig_vectors[:, idxs[::-1]]
    return (np.multiply(eig_values, eig_vectors))[:, :q], eig_values[:q], eig_vectors[:, :q]
