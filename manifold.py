import numpy as np
from sklearn import datasets
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from eigenmap import Eigenmap
from diffusion import Diffusion


# print("Weight matrix")
# print(W)
# print("Degree matrix")
# print(D)
# print("Laplacian matrix")
# print(L)
# print("Mapping")
# print(X)


np.set_printoptions(linewidth=200)
swiss, colour = datasets.make_swiss_roll(1000)

# Construct Diffusion map for swiss roll
diff = Diffusion(swiss)

# Add point and perform out of sample extension
point = np.array([[0, 8, -5], [0, 8, 8], [0, 8, 15]])
swiss = np.vstack((swiss, point))
print(colour.shape)
colour = np.append(colour, [20, 25, 30])
print(colour.shape)
diff.add_point(point[0])
diff.add_point(point[1])
diff.add_point(point[2])

# Generate map for dataset
X = diff.get_map(q=3)

fig = plt.figure()
ax = fig.add_subplot(211, projection='3d')
ax.scatter(swiss[:, 0], swiss[:, 1], swiss[:, 2], c=colour, cmap=plt.cm.Spectral)
ax = fig.add_subplot(212)
ax.scatter(X[:, 0], X[:, 1], c=colour, cmap=plt.cm.Spectral)

plt.show()