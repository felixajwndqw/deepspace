import numpy as np
import graph
from eigenmap import generate_mapping
from scipy.spatial.distance import cdist


class Diffusion:
    def __init__(self, data):
        self.data = data
        self.W, self.sigma = graph.create_weight_matrix(data, density_invariant=True)
        self.D = graph.create_degree_matrix(self.W)
        self.L = create_transition_matrix(self.W, self.D)
        self.orig_dim = data.shape[1]
        self.X, self.eig_values, self.eig_vectors = generate_mapping(self.L, self.orig_dim)

    def get_map(self, q):
        return self.X[:, :q]
    
    def add_point(self, point):
        oos = out_of_sample(point, self.data, self.eig_values,
                            self.eig_vectors, self.sigma)
        print("New sample is ", oos)
        self.X = np.vstack((self.X, oos))

    # def get_near_manifold(self, point):


def create_transition_matrix(W, D):
    return (W/np.diag(D))


# def neighbouring_manifold(point, )
def diff_dist(XA, XB):
    if XA.shape[0] < 2 or XB.shape[0] < 2:
        return np.sum((XA-XB)**2, axis=1)


# Returns k neighbouring points of a given sample
def neighbouring_samples(point, data, k, dist_function=diff_dist):
    if point.shape.size < 2:
        point = np.array([point])
    dists = dist_function(point, data)
    return data[knn_dists(dists, k)]


def knn_dists(dists, k):
    idxs = dists.argsort()
    if dists[idxs[0]] == 0:
        return idxs[1:k+1]
    return idxs[:k]


def out_of_sample(point, t_data, eig_values, eig_vectors, sigma=1):
    dists = cdist([point], t_data).flatten()
    k = 5
    idxs = knn_dists(dists, k)
    
    print(dists)
    dists = dists[idxs]
    eig_vectors = eig_vectors[idxs]
    print(dists)

    # Normalise new point
    W = graph.gaussian_weight(dists, sigma)
    p = W/np.sum(W)
    print(p)
    p = p.reshape((p.shape[0], 1))
    ev = np.sum(np.multiply(eig_vectors, p), axis=0)/eig_values


    return ev
